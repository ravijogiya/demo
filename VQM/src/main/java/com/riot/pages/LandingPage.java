package com.riot.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.framework.actions.WebAction;
import com.functions.CommonFunctionLib;
import com.riot.pageObjects.LandingPageObjects;

public class LandingPage {

	protected WebDriver driver;
	protected WebAction action;
	private LandingPageObjects obj;
	
	public LandingPage(WebDriver driver, WebAction action) {
		this.driver = driver;
		this.action = action;
		obj = new LandingPageObjects(driver);
	}
	
	public HomePage login(String userId, String password) throws Exception {
		CommonFunctionLib.log("Login to RIOT", driver);
		loginMethod(userId, password);

		return new HomePage(driver, action);
	}
	

	private void loginMethod(String userId, String password) throws IOException, Exception {
		if (!userId.equals("") && !password.equals("")) {
			//verifyLandingPage();

			System.out.println("UserId: " + userId);

			action.inputText(obj.txtNTid, userId);
			CommonFunctionLib.log("Enter User Id: '" + userId + "'", driver);

			action.inputText(obj.txtPassword, password);
			CommonFunctionLib.log("Enter Password: '" + password + "'", driver);

			action.click(obj.btnSignIn);
			CommonFunctionLib.log("Click on 'Login' button", driver);
		}
	}


}
