package com.riot.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPageObjects {
	
	public LandingPageObjects (WebDriver driver)	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//*[@id=\"Username\"]")
	public WebElement txtNTid;
	
	@FindBy(xpath="//*[@id=\"Password\"]")
	public WebElement txtPassword;
	
	@FindBy (xpath="//*[@id=\"btnSignin\"]")
	public WebElement btnSignIn;
	

}
