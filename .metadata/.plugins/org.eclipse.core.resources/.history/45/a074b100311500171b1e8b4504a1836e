/**
 * 
 */
package com.functions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.SkipException;

import com.framework.actions.ErrorHandalingMethod;
import com.framework.actions.WaitMethods;
import com.framework.actions.WebAction;

/**
 * CommonFunctionLib.java contains general purpose functions
 */
public class CommonFunctionLib {
	public WebDriver driver;
	WebDriverWait wait;
	Properties properties;
	ChromeOptions chromeoptions;
	public Set<String> arrKnownBrowserHwnd;
	public String hwndFirstWindow;
	public String hwndMostRecentWindow;
	public Boolean locationServiceEnabled;
	public Boolean doFullReset;
	public static int imageCount = 0;
	public static String userDir = System.getProperty("user.dir");
	public static int productId = 0;
	public static int catId = 1;
	public static String orgName = "";
	public static int orgNumber;
	public static Hashtable<String, String> newOrgDetails = new Hashtable<>();
	static WebDriver driver2;

	/**
	 * Purpose : Constructor with WebDriver argument
	 * 
	 * @param driver
	 */
	public CommonFunctionLib(WebDriver driver) {
		this.driver = driver;
		locationServiceEnabled = Boolean
				.parseBoolean(properties.getProperty("locationServiceEnabled").trim().toLowerCase());
		doFullReset = true;
	}

	/**
	 * Purpose : Constructor with no argument
	 */
	public CommonFunctionLib() {

	}

	public static void sleep(int secounds) {
		try {
			Thread.sleep(secounds * 1000);
		} catch (InterruptedException e) {
		}
	}

	public static void assertTrue(boolean condition, String msg, WebDriver driver) throws Exception, IOException {
		if (!condition) {
			log("Failed log: " + msg, driver);
			throw new Exception("Functional Fail: " + msg);
		}
	}

	public static String getTodayDate() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		String strDay = "";
		String strMonth = "";
		if (day < 10) {
			strDay = "0" + Integer.toString(day);
		} else {
			strDay = Integer.toString(day);
		}

		int year = calendar.get(Calendar.YEAR);

		int month = calendar.get(Calendar.MONTH);

		if (month < 10) {
			strMonth = "0" + Integer.toString(month + 1);
		} else {
			strMonth = Integer.toString(month + 1);
		}

		return strMonth + "/" + strDay + "/" + year;
	}

	public static String readFromConsole(String text) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(text);

		return br.readLine();
	}

	public static void assertEquals(String actual, String expected, String msg, WebDriver driver)
			throws Exception, IOException {
		if (!actual.equals(expected)) {
			log("Failed log: " + msg, driver);
			throw new Exception("Functional Fail: " + msg);
		}
	}

	/**
	 * Purpose : This function injects file path into Windows File Upload dialog
	 * 
	 * @param filePath
	 * @throws AWTException
	 */
	static Robot robot;

	public static void UploadFile(String filePath) throws AWTException {
		StringSelection stringSelection = new StringSelection(filePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		robot = new Robot();
		robot.delay(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(500);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.delay(500);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.delay(500);
		robot.keyPress(KeyEvent.VK_V);
		robot.delay(500);
		robot.keyRelease(KeyEvent.VK_V);
		robot.delay(500);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.delay(500);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(500);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public static void keyPress(int keycode, int count) {
		for (int i = 0; i < count; i++) {
			try {
				robot = new Robot();
			} catch (AWTException e) {
				e.printStackTrace();
			}
			robot.keyPress(keycode);
			sleep(1);
			robot.keyRelease(keycode);
		}
	}

	public static void copyToClipboard(String text) {
		StringSelection stringSelection = new StringSelection(text);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);
	}

	/***
	 * 
	 * @param location
	 */
	public void CreateFolder(String location) {
		File Log = new File(location);
		if (Log.exists()) {
		} else {
			new File(location).mkdir();
		}
	}

	public static String getCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		String strDay = "";
		if (day < 10)
			strDay = "0" + Integer.toString(day);

		int year = calendar.get(Calendar.YEAR);

		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();

		int month = calendar.get(Calendar.MONTH);

		int i = 3;
		String strMonth = months[month].substring(0, i) + months[month].substring(i + 2);

		return strDay + strMonth + year;
	}

	public static void tryAgain(ErrorHandalingMethod method, int seconds) throws Exception {
		int count = 0;
		while (true) {
			try {
				method.errorHandlingMethod();
				break;
			} catch (Exception e) {
				CommonFunctionLib.sleep(1);
				count++;
				if (count == seconds) {
					count = 0;
					throw new Exception(e.getLocalizedMessage());
				}
			}
		}
	}

	public static void tryAgain(ErrorHandalingMethod method, int seconds, Boolean val) throws Exception {
		int count = 0;
		while (true) {
			try {
				method.errorHandlingMethod();
				break;
			} catch (Exception e) {
				CommonFunctionLib.sleep(1);
				count++;
				if (count == seconds) {
					count = 0;
					if (val) {
						throw new Exception(e.getLocalizedMessage());
					}
				}
			}
		}
	}

	// method for add log to report
	public static void log(String log, WebDriver driver) throws Exception, IOException {
		if (driver != null) {
			tryAgain(() -> {
				System.out.println(driver + ": " + log);
				Reporter.log("<a href=\"" + new CaptureBrowserScreenShot().takeScreenShots(driver) + "\"> " + log
						+ "</a> <br />");
			}, 3);
		}
	}

	public static void skip(String testName) {
		throw new SkipException("Skipping " + testName + " as browser is not selected from test execution platform");
	}

	public static int randInt(int min, int max) {
		// Usually this can be a field rather than a method variable
		Random rand = new Random();
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public static void confirmSecurityException(WebAction action, WebDriver driver) throws Exception, IOException {
		CommonFunctionLib.sleep(1);
		try {
			action.click(driver.findElement(By.id("advancedButton")));
			CommonFunctionLib.log("Click on 'Advance' button", driver);

			action.click(driver.findElement(By.id("exceptionDialogButton")));
			CommonFunctionLib.log("Click on 'Add Exception' button", driver);

			CommonFunctionLib.keyPress(KeyEvent.VK_TAB, 4);
			CommonFunctionLib.keyPress(KeyEvent.VK_ENTER, 1);
			CommonFunctionLib.log("Confirm Secutity Exception", driver);
		} catch (Exception e) {
		}
	}

	public static void setimplicitlyWait(int seconds, WebDriver driver) {
		driver2 = driver;
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}

	public static void waitUntilTrue(WaitMethods wait, int seconds) {
		int count = 0;
		setimplicitlyWait(1, driver2);

		while (true) {
			if (wait.trueCondition()) {
				break;
			} else {
				CommonFunctionLib.sleep(1);
				count++;
				if (count == seconds) {
					count = 0;
					break;
				}
			}
		}
		setimplicitlyWait(15, driver2);
	}

	public static void waitUntilFalse(WaitMethods wait, int seconds) {
		setimplicitlyWait(1, driver2);
		int count = 0;
		while (true) {
			System.err.println("val: " + wait.trueCondition());
			if (!wait.trueCondition()) {
				break;
			} else {
				CommonFunctionLib.sleep(1);
				count++;
				if (count == seconds) {
					count = 0;
					break;
				}
			}
		}
		setimplicitlyWait(15, driver2);
	}
}